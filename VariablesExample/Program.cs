﻿using System;

namespace VariablesExample
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Declare a variable and assign afterwards
             */
            string myFirstString;
            myFirstString = "I am ascending";

            /*
             * Declare some variables, of all types, with assignment on declaration
             *  -string
             *  -char
             *  -int
             *  -long
             *  -double
             *  -float
             *  -bool
             * Show differences between similar datatypes
             */
            string myOtherString = "Hello, I am a sentance";
            char myChar = 'a';
            int myInt = 121312;
            long myLong = 12131231241241;
            double myDouble = 2.2453463689;
            float myFloat = 2.2453463689F;
            bool myBool = true;
            Console.WriteLine(myFloat);

            /*
             * Show Intellisense for the varibales declared above
             */
            int length = myOtherString.Length;
            Console.WriteLine(myOtherString.Substring(0, 5));
            string trimmed = myOtherString.Trim();
            int res = myOtherString.CompareTo("Not the same");
            Console.WriteLine(res);
            myOtherString.Split(' ');

            /*
             * Demonstrate implicit (automatic) casting
             * Converting smaller types to larger
             *  - int > double
             *  - float > double
             *  - int > long
             *  - More, just have to play around with it
             */
            double convertedInt = myInt;
            double convertedFloat = myFloat;
            long convertIn2 = myLong;

            /*
             * Demonstrate explicit casting
             * Converting larger to smaller, makes some changes, which is why its manually done
             *  - double > int (chops off decimal places)
             *  - long > int (weird things happen here, try avoid)
             *  - Many more, will discover while creating programs
             */
            int convertedDouble = (int)myDouble;
            Console.WriteLine(convertedDouble);
            int convertedLong = (int)myLong;
            Console.WriteLine(convertedLong);

            /*
             * Type conversion methods
             *  - Convert.ToX()
             *  - Numbers to bool with Convert.ToBoolean (any number but 0 is true, 0 is false)
             */
            bool convertedInt2Bool = Convert.ToBoolean(0);
            Console.WriteLine(convertedInt2Bool);

        }
    }
}
